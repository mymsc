PRINCIPAL= mscMonografia

TEXTOS= $(PRINCIPAL).tex
XFIGFIGURAS=
DIAFIGURAS=
SLIDES=

.PHONY : pdf ps dvi slides clean


pdf : $(PRINCIPAL).pdf

$(PRINCIPAL).pdf : $(PRINCIPAL).ps
	ps2pdf $(PRINCIPAL).ps


ps : $(PRINCIPAL).ps

$(PRINCIPAL).ps : $(PRINCIPAL).dvi
	dvips -P pdf $(PRINCIPAL).dvi


dvi : $(PRINCIPAL).dvi

$(PRINCIPAL).dvi : $(DIAFIGURAS) $(XFIGFIGURAS) $(PRINCIPAL).bbl $(TEXTOS)
	latex $(PRINCIPAL).tex ; latex $(PRINCIPAL).tex


%.dia.eps : figuras/%.dia
	dia -n -e $@ figuras/`basename $@ .eps`

%.fig.eps : figuras/%.fig
	fig2dev -L eps figuras/`basename $@ .eps` $@

$(PRINCIPAL).bbl : $(PRINCIPAL).bib
	latex $(PRINCIPAL).tex ; bibtex $(PRINCIPAL) ; latex $(PRINCIPAL).tex


slides : $(SLIDES).pdf

$(SLIDES).pdf : $(FIGURAS) $(SLIDES).tex
	latex $(SLIDES).tex ; latex $(SLIDES).tex ; \
        dvips -P pdf $(SLIDES).dvi ; ps2pdf $(SLIDES).ps


clean :
	rm -f `tail -n +2 .gitignore`
